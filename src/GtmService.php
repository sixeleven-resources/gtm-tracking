<?php

/**
 * @file
 * Contains \Drupal\gtm_tracking\GtmService.
 */

namespace Drupal\gtm_tracking;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * GtmController.
 */
class GtmService {

  /**
   * PrivateTempStore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  public $tempStore;

  /**
   * PrivateTempStore.
   *
   * @var \Drupal\Core\TempStore\SharedTempStoreFactory
   */
  public $sharedTempStore;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  public $account;


  /**
   * GtmController constructor.
   */
  public function __construct(PrivateTempStoreFactory $privateTempStore, SharedTempStoreFactory $sharedTempStore, AccountProxyInterface $account) {
    $this->privateTempStore = $privateTempStore;
    $this->sharedTempStore = $sharedTempStore;
    $this->currentUser = $account;
  }

  public function updateStore($id, $data, $type = 'private', $uid = NULL) {
    $parameters = $this->getParameters($type, $id, $uid);

    $property = $parameters['property'];
    $id = $parameters['id'];

    $store = $this->$property->get('gtm_tracking');

    $gtm_events = $store->get($id);
    if ($gtm_events === null) {
      $gtm_events = [];
    }

    $gtm_events[] = $data;
    $store->set($id, $gtm_events);
  }

  public function getStore($id, $type = 'private', $uid = NULL) {
    $parameters = $this->getParameters($type, $id, $uid);

    $property = $parameters['property'];
    $id = $parameters['id'];

    $store = $this->$property->get('gtm_tracking');
    return $store->get($id);
  }

  public function overrideStore($id, $gtm_events, $type = 'private', $uid = NULL) {
    $parameters = $this->getParameters($type, $id, $uid);

    $property = $parameters['property'];
    $id = $parameters['id'];

    $store = $this->$property->get('gtm_tracking');

    $store->set($id, $gtm_events);
  }

  public function deleteStore($id, $type = 'private', $uid = NULL) {
    $parameters = $this->getParameters($type, $id, $uid);

    $property = $parameters['property'];
    $id = $parameters['id'];

    $store = $this->$property->get('gtm_tracking');
    $store->delete($id);
  }

  public function getParameters($type, $id, $uid) {
    $parameters['property'] = 'privateTempStore';
    $parameters['id'] = $id;

    if ($type === 'shared') {
      $parameters['property'] = 'sharedTempStore';

      if (!isset($uid)) {
        $current_user = $this->currentUser;
        $uid = $current_user->id();
      }

      $parameters['id'] = $id . '_' . $uid;
    }

    return $parameters;
  }

}
