<?php

namespace Drupal\gtm_tracking\EventSubscriber;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * GTM Tracking event subscriber.
 */
class GtmTrackingSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Renders the ajax commands right before preparing the result.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The response event, which contains the possible AjaxResponse object.
   */
  public function onResponse(ResponseEvent $event) {
    $response = $event->getResponse();
    if ($response instanceof AjaxResponse) {
      $gtm_service = \Drupal::service('gtm_tracking.gtm_service');
      $events = $gtm_service->getStore('gtm_events');
      $shared_events = $gtm_service->getStore('gtm_events', 'shared');
      if (!empty($events) || !empty($shared_events)) {
        if (!empty($shared_events)) {
          if (!empty($events)) {
            $events = array_merge($events, $shared_events);
          }
          else {
            $events = $shared_events;
          }
        }

        $attachments = $response->getAttachments();

        $attachments['drupalSettings']['gtm_events'] = $events;
        $response->setAttachments($attachments);
        $event->setResponse($response);

        $gtm_service->deleteStore('gtm_events');
        $gtm_service->deleteStore('gtm_events', 'shared', $gtm_service->currentUser->id());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::RESPONSE => [['onResponse']]];
  }

}
