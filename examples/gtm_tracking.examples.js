/**
 * @file
 */
(function ($, Drupal) {

  'use strict';
  window.dataLayer = window.dataLayer || [];

  Drupal.behaviors.gtm = {
    attach: function (context, settings) {

      $(document).on('click', '.form-submit', function (e) {
        if (!$(this).hasClass('gtm-processed')) {
          gtag('event', 'form_submit', {type: 'contact_us'});
          // Or:
          window.dataLayer.push({
            event: 'form_submit',
            type: 'contact_us',
          });

          //You can use both gtag() or window.dataLayer.push() version.

          $(this).addClass('gtm-processed');
        }
      });

      // File download section.
      // This code is related to the "File download section" in the
      // examples/gtm_tracking.examples.php file.
      $(document).on('click', '.gtm-download a', function (e) {
        if (!$(this).hasClass('gtm-processed')) {
          let fileWrapper = $(this).closest('.gtm-download');

          // Same here, use gtag OR dataLayers.
          gtag('event', 'file_download', {
            file_extension: fileWrapper.data('file-extension'),
            file_name: fileWrapper.data('file-name'),
            link_text: fileWrapper.data('link-text'),
            link_url: fileWrapper.data('link-url'),
          });

          window.dataLayer.push({
            event: 'file_download',
            file_extension: fileWrapper.data('file-extension'),
            file_name: fileWrapper.data('file-name'),
            link_text: fileWrapper.data('link-text'),
            link_url: fileWrapper.data('link-url'),
          });

          $(this).addClass('gtm-processed');
        }
      });

    }
  };

})(jQuery, Drupal);
