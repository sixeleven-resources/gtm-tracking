<?php

/**
 * Working examples.
 * Copy/paste and edit function name into your other modules to use.
 */

use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * Implements hook_page_attachments().
 */
function gtm_tracking_page_attachments(&$variables) {
  $current_user = \Drupal::currentUser();

  $gtm_service = \Drupal::service('gtm_tracking.gtm_service');
  $data = [
    'type' => 'event',
    'value' => 'attributes_push',
    'parameters' => [
      'user_id'=> $current_user->id()
    ],
  ];

  $gtm_service->updateStore('gtm_events', $data);
}

/**
 * Implements hook_user_login().
 */
function gtm_tracking_user_login(UserInterface $account) {
  $gtm_service = \Drupal::service('gtm_tracking.gtm_service');
  $data = [
    'type' => 'event',
    'value' => 'login'
  ];

  $gtm_service->updateStore('gtm_events', $data);
}

/**
 * Implements hook_entity_insert().
 */
function gtm_tracking_entity_insert(\Drupal\Core\Entity\EntityInterface $entity) {
  if ($entity->bundle() === 'user') {
    $gtm_service = \Drupal::service('gtm_tracking.gtm_service');
    $data = [
      'type' => 'event',
      'value' => 'sign_up',
    ];

    $gtm_service->updateStore('gtm_events', $data, 'shared', $entity->id());
  }
}

/**
 * Implements hook_preprocess_file_link().
 */
function gtm_tracking_preprocess_file_link(&$variables) {
  // File download section.
  // This code is related to the "File download section" in the
  // examples/gtm_tracking.examples.js file.
  if (isset($variables["file"]) and $variables["file"] instanceof File) {
    $file     = $variables["file"];
    $pathinfo = pathinfo($file->getFilename());

    $variables["attributes"]['class']               = 'gtm-download';
    $variables["attributes"]['data-file-extension'] = $pathinfo['extension'];
    $variables["attributes"]['data-file-name']      = $variables["file"]->filename->value;

    if (isset($variables["link"]["#title"])) {
      $variables["attributes"]['data-link-text'] = $variables["link"]["#title"];
    }

    if (isset($variables["link"]["#url"]) and $variables["link"]["#url"] instanceof Url) {
      $variables["attributes"]['data-link-url'] = $variables["link"]["#url"]->toString();
    }
  }
}
