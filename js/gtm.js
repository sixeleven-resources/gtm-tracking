/**
 * @file
 */
(function ($, Drupal) {

  'use strict';
  window.dataLayer = window.dataLayer || [];

  Drupal.behaviors.gtm = {
    attach: function (context, settings) {

      if (settings.gtm_events !== undefined) {
        settings.gtm_events.forEach(function(data, index) {
          gtag(data.type, data.value, data.parameters);
        });
        settings.gtm_events = undefined;
      }

    }
  };

})(jQuery, Drupal);
